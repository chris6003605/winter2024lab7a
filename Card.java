public class Card{
	private String suit;
	private String value;
	
	
	public Card(String suit, String value){
		this.suit = suit;
		this.value = value;
	}
	
	public String getSuit(){
		return this.suit;
	}
	public String getValue(){
		return this.value;
	}
	
	public double calculateScore(){
			double score = 0;
			//calculate the score based on the value
			String[] VALUES = { "Ace", "Two",  "Three", "Four", "Five", "Six", "Seven", "Eight",  "Nine", "Ten", "Jack", "Queen", "King"};
			for(int i =0; i < VALUES.length; i++){
				if(this.value.equals(VALUES[i])){
				score = i + 1;
				}
			}
			
			if( suit.equals("Hearts ")){
			score += 0.4; 
			
			}
			else if(suit.equals("Spades ")){
				score += 0.3; 
				
			}
			else if(suit.equals("Diamonds ")){
				score += 0.2;
				
			}
			else{
				score += 0.1;
				
			}
			return score;
	}
		
	
	
	public String toString(){
		return this.value + " of " + this.suit; 
	}
}